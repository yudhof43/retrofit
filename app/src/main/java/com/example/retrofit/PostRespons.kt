package com.example.retrofit


data class PostRespons{
    val id: Int,
    val title: String?.
    @SerializedName("body")
    val text : String?
}